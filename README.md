## Filtring and Upsampling using Bilateral Filters

### Gaussian filter

Smoothing image after contaminating it with additive noise using the gaussian filter.

noisy | K = 7, &sigma;=0.4 | K = 7, &sigma;=0.8
:-: | :-: | :-:
![](./output/gaussian/Art/noisy.png) | ![](./output/gaussian/Art/smoothed_k_7_s_0.4.png) | ![](./output/gaussian/Art/smoothed_k_7_s_0.8.png) 
K = 7, &sigma;=1.2 | K = 7, &sigma;=2
![](./output/gaussian/Art/smoothed_k_7_s_1.2.png) | ![](./output/gaussian/Art/smoothed_k_7_s_2.png) 

noisy | K = 7, &sigma;=0.4 | K = 7, &sigma;=0.8
:-: | :-: | :-:
![](./output/gaussian/Dolls/noisy.png) | ![](./output/gaussian/Dolls/smoothed_k_7_s_0.4.png) | ![](./output/gaussian/Dolls/smoothed_k_7_s_0.8.png) 
K = 7, &sigma;=1.2 | K = 7, &sigma;=2
![](./output/gaussian/Dolls/smoothed_k_7_s_1.2.png) | ![](./output/gaussian/Dolls/smoothed_k_7_s_2.png) 


### Bilateral Filter

Smoothing image after contaminating it with additive noise using the Bilateral Filter.

K = 7, &sigma;1=0.4, &sigma;2=10 | K = 7, &sigma;1=0.4, &sigma;2=15
:-: | :-: 
![](./output/bilateral/Dolls/smoothed_k_7_s1_0.4%20_s2_10.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_0.4%20_s2_15.png)
K = 7, &sigma;1=0.4, &sigma;2=20 | K = 7, &sigma;1=0.4, &sigma;2=25
![](./output/bilateral/Dolls/smoothed_k_7_s1_0.4%20_s2_15.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_0.4%20_s2_20.png)
K = 7, &sigma;1=0.8, &sigma;2=10 | K = 7, &sigma;1=0.8, &sigma;2=15
![](./output/bilateral/Dolls/smoothed_k_7_s1_0.8%20_s2_10.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_0.8%20_s2_15.png)
K = 7, &sigma;1=0.8, &sigma;2=20 | K = 7, &sigma;1=0.8, &sigma;2=25
![](./output/bilateral/Dolls/smoothed_k_7_s1_0.8%20_s2_15.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_0.8%20_s2_20.png)
K = 7, &sigma;1=1.2, &sigma;2=10 | K = 7, &sigma;1=1.2, &sigma;2=15
![](./output/bilateral/Dolls/smoothed_k_7_s1_1.2%20_s2_10.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_1.2%20_s2_15.png)
K = 7, &sigma;1=1.2, &sigma;2=20 | K = 7, &sigma;1=1.2, &sigma;2=25
![](./output/bilateral/Dolls/smoothed_k_7_s1_1.2%20_s2_15.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_1.2%20_s2_20.png)
K = 7, &sigma;1=2, &sigma;2=10 | K = 7, &sigma;1=2, &sigma;2=15
![](./output/bilateral/Dolls/smoothed_k_7_s1_2%20_s2_10.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_2%20_s2_15.png)
K = 7, &sigma;1=2, &sigma;2=20 | K = 7, &sigma;1=2, &sigma;2=25
![](./output/bilateral/Dolls/smoothed_k_7_s1_2%20_s2_15.png) | ![](./output/bilateral/Dolls/smoothed_k_7_s1_2%20_s2_20.png)

<b> Note: </b> We can notice that the bilateral filter preserves the edges of the image even using the Gaussian filter with large &sigma;. 

### Guided Bilateral Filter

Smoothing image after contaminating it with additive noise using the Guided Bilateral Filter, where the guide is the disparity image.

K = 7, &sigma;1=0.4, &sigma;2=10 | K = 7, &sigma;1=0.4, &sigma;2=15
:-: | :-: 
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.4%20s2_10.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.4%20s2_15.png)
K = 7, &sigma;1=0.4, &sigma;2=20 | K = 7, &sigma;1=0.4, &sigma;2=25
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.4%20s2_15.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.4%20s2_20.png)
K = 7, &sigma;1=0.8, &sigma;2=10 | K = 7, &sigma;1=0.8, &sigma;2=15
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.8%20s2_10.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.8%20s2_15.png)
K = 7, &sigma;1=0.8, &sigma;2=20 | K = 7, &sigma;1=0.8, &sigma;2=25
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.8%20s2_15.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_0.8%20s2_20.png)
K = 7, &sigma;1=1.2, &sigma;2=10 | K = 7, &sigma;1=1.2, &sigma;2=15
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_1.2%20s2_10.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_1.2%20s2_15.png)
K = 7, &sigma;1=1.2, &sigma;2=20 | K = 7, &sigma;1=1.2, &sigma;2=25
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_1.2%20s2_15.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_1.2%20s2_20.png)
K = 7, &sigma;1=2, &sigma;2=10 | K = 7, &sigma;1=2, &sigma;2=15
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_2%20s2_10.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_2%20s2_15.png)
K = 7, &sigma;1=2, &sigma;2=20 | K = 7, &sigma;1=2, &sigma;2=25
![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_2%20s2_15.png) | ![](./output/bilateral_joint/Dolls/smoothed_k_7_s1_2%20s2_20.png)

<b> Note </b>: Using the Guided Bilateral Filter, better noise elimination and sharp edges are retrieved.

### The Bilateral Median Filter

Smoothing image using the Bilateral Median Filter.

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median/Art/smoothed_k_7_s_10.png) | ![](./output/bilateral_median/Art/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median/Art/smoothed_k_7_s_20.png) | ![](./output/bilateral_median/Art/smoothed_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median/Dolls/smoothed_k_7_s_10.png) | ![](./output/bilateral_median/Dolls/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median/Dolls/smoothed_k_7_s_20.png) | ![](./output/bilateral_median/Dolls/smoothed_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median/Moebius/smoothed_k_7_s_10.png) | ![](./output/bilateral_median/Moebius/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median/Moebius/smoothed_k_7_s_20.png) | ![](./output/bilateral_median/Moebius/smoothed_k_7_s_25.png)


### The Guided Bilateral Median Filter
Smoothing image after contaminating it with additive noise using the Guided Bilateral Median Filter, where the guide is the disparity image.

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median_joint/Art/smoothed_k_7_s_10.png) | ![](./output/bilateral_median_joint/Art/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median_joint/Art/smoothed_k_7_s_20.png) | ![](./output/bilateral_median_joint/Art/smoothed_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median_joint/Dolls/smoothed_k_7_s_10.png) | ![](./output/bilateral_median_joint/Dolls/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median_joint/Dolls/smoothed_k_7_s_20.png) | ![](./output/bilateral_median_joint/Dolls/smoothed_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_median_joint/Moebius/smoothed_k_7_s_10.png) | ![](./output/bilateral_median_joint/Moebius/smoothed_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_median_joint/Moebius/smoothed_k_7_s_20.png) | ![](./output/bilateral_median_joint/Moebius/smoothed_k_7_s_25.png)

### Opencv Interpolation Upsampling
The depth image is downsampled with scale of 4 and upsampled using the bilinear, cubic, and nearest neighbours interploation techniques.

Downsampled | Bilinear Interpolation 
:-: | :-: 
![](./output/bilinear_upsampling/Art/downsampled.png) | ![](./output/bilinear_upsampling/Art/upsampled.png)
Cubic Interpolation | NN Interpolation 
![](./output/cubic_upsampling/Art/upsampled.png) | ![](./output/nn_upsampling/Art/upsampled.png)


Downsampled | Bilinear Interpolation 
:-: | :-: 
![](./output/bilinear_upsampling/Dolls/downsampled.png) | ![](./output/bilinear_upsampling/Dolls/upsampled.png)
Cubic Interpolation | NN Interpolation 
![](./output/cubic_upsampling/Dolls/upsampled.png) | ![](./output/nn_upsampling/Dolls/upsampled.png)


Downsampled | Bilinear Interpolation 
:-: | :-: 
![](./output/bilinear_upsampling/Moebius/downsampled.png) | ![](./output/bilinear_upsampling/Moebius/upsampled.png)
Cubic Interpolation | NN Interpolation 
![](./output/cubic_upsampling/Moebius/upsampled.png) | ![](./output/nn_upsampling/Moebius/upsampled.png)

### Iterative Upsampling using the Guided Bilateral Filter
The depth image upsampling using the Iterative Guided Bilateral Filter, where the guide is the RGB image.

K = 7, &sigma;1=0.4, &sigma;2=10 | K = 7, &sigma;1=0.4, &sigma;2=15
:-: | :-: 
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.4%20s2_10.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.4%20s2_15.png)
K = 7, &sigma;1=0.4, &sigma;2=20 | K = 7, &sigma;1=0.4, &sigma;2=25
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.4%20s2_15.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.4%20s2_20.png)
K = 7, &sigma;1=0.8, &sigma;2=10 | K = 7, &sigma;1=0.8, &sigma;2=15
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.8%20s2_10.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.8%20s2_15.png)
K = 7, &sigma;1=0.8, &sigma;2=20 | K = 7, &sigma;1=0.8, &sigma;2=25
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.8%20s2_15.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_0.8%20s2_20.png)
K = 7, &sigma;1=1.2, &sigma;2=10 | K = 7, &sigma;1=1.2, &sigma;2=15
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_1.2%20s2_10.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_1.2%20s2_15.png)
K = 7, &sigma;1=1.2, &sigma;2=20 | K = 7, &sigma;1=1.2, &sigma;2=25
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_1.2%20s2_15.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_1.2%20s2_20.png)
K = 7, &sigma;1=2, &sigma;2=10 | K = 7, &sigma;1=2, &sigma;2=15
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_2%20s2_10.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_2%20s2_15.png)
K = 7, &sigma;1=2, &sigma;2=20 | K = 7, &sigma;1=2, &sigma;2=25
![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_2%20s2_15.png) | ![](./output/bilateral_joint_upsampling/Dolls/upsampled_k_7_s1_2%20s2_20.png)


### Iterative Upsampling using the Guided Median Bilateral Filter
The depth image upsampling using the Iterative Guided Median Bilateral Filter, where the guide is the RGB image.


K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_joint_median_upsampling/Art/upsampled_k_7_s_10.png) | ![](./output/bilateral_joint_median_upsampling/Art/upsampled_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_joint_median_upsampling/Art/upsampled_k_7_s_20.png) | ![](./output/bilateral_joint_median_upsampling/Art/upsampled_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_joint_median_upsampling/Dolls/upsampled_k_7_s_10.png) | ![](./output/bilateral_joint_median_upsampling/Dolls/upsampled_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_joint_median_upsampling/Dolls/upsampled_k_7_s_20.png) | ![](./output/bilateral_joint_median_upsampling/Dolls/upsampled_k_7_s_25.png)

K = 7, &sigma;=10 | K = 7, &sigma;=15
:-: | :-: 
![](./output/bilateral_joint_median_upsampling/Moebius/upsampled_k_7_s_10.png) | ![](./output/bilateral_joint_median_upsampling/Moebius/upsampled_k_7_s_15.png)
K = 7, &sigma;=20 | K = 7, &sigma;=25
![](./output/bilateral_joint_median_upsampling/Moebius/upsampled_k_7_s_20.png) | ![](./output/bilateral_joint_median_upsampling/Moebius/upsampled_k_7_s_25.png)


## Conclusion
- Noise Filtering: the Guided Bilateral Median Filter returns the best results compared with other smoothing methods, the results are smooth, noise is eliminated, and the edges are preserved. 

- Upsampling: the Iterative Upsampling using the Guided Median Bilateral Filter returns the best results compared with other interpolation techniques and the Iterative Upsampling using the Guided Bilateral Filter. The results are smooth, and edges are preserved.