# Gaussian filter
'''
mkdir "output/gaussian"
for filename in data/*/view1.png; do  
    mkdir "output/gaussian/$(echo $filename | cut -f 2 -d '/')"
    n="output/gaussian/$(echo $filename | cut -f 2 -d '/')/noisy.png"
    
    declare -a arr=("0.4" "0.8" "1.2" "2")
    
    for i in "${arr[@]}"; do
        ./build/filters 0 $filename $n "output/gaussian/$(echo $filename | cut -f 2 -d '/')/smoothed_k_7_s_$i.png" 7 $i
    done
done


# Bilateral filter
mkdir "output/bilateral"
for filename in data/*/view1.png; do
    mkdir "output/bilateral/$(echo $filename | cut -f 2 -d '/')"

    declare -a arr=("0.4" "0.8" "1.2" "2")
    declare -a arr2=("10" "15" "20" "25")


    n="output/bilateral/$(echo $filename | cut -f 2 -d '/')/noisy.png"

    for i in "${arr[@]}"; do
        for j in "${arr2[@]}"; do
            ./build/filters 1 $filename $n "output/bilateral/$(echo $filename | cut -f 2 -d '/')/smoothed_k_7_s1_$i _s2_$j.png" 7 $i $j
        done
    done
done


# Bilateral joint filter
mkdir "output/bilateral_joint"
for filename in data/*/view1.png; do
    mkdir "output/bilateral_joint/$(echo $filename | cut -f 2 -d '/')"

    declare -a arr=("0.4" "0.8" "1.2" "2")
    declare -a arr2=("10" "15" "20" "25")

    n="output/bilateral_joint/$(echo $filename | cut -f 2 -d '/')/noisy.png"

    for i in "${arr[@]}"; do
        for j in "${arr2[@]}"; do
            ./build/filters 2 $filename $n "output/bilateral_joint/$(echo $filename | cut -f 2 -d '/')/smoothed_k_7_s1_$i s2_$j.png" 7 $i $j "$(dirname $(echo $filename))/disp1.png"
        done
    done
done

# Bilateral median filter
mkdir "output/bilateral_median"
for filename in data/*/view1.png; do
    mkdir "output/bilateral_median/$(echo $filename | cut -f 2 -d '/')"

    n="output/bilateral_median/$(echo $filename | cut -f 2 -d '/')/noisy.png"

    declare -a arr2=("10" "15" "20" "25")

    for j in "${arr2[@]}"; do
        ./build/filters 3 $filename $n "output/bilateral_median/$(echo $filename | cut -f 2 -d '/')/smoothed_k_7_s_$j.png" 7 0 $j
    done
done
'''

# Bilateral median joint filter
mkdir "output/bilateral_median_joint"
for filename in data/*/view1.png; do
    mkdir "output/bilateral_median_joint/$(echo $filename | cut -f 2 -d '/')"

    n="output/bilateral_median_joint/$(echo $filename | cut -f 2 -d '/')/noisy.png"

    declare -a arr2=("10" "15" "20" "25")

    for j in "${arr2[@]}"; do
        ./build/filters 4 $filename $n "output/bilateral_median_joint/$(echo $filename | cut -f 2 -d '/')/smoothed_k_7_s_$j.png" 7 0 $j "$(dirname $(echo $filename))/disp1.png"
    done
done

'''
# Opencv upsampling
mkdir "output/bilinear_upsampling"
mkdir "output/cubic_upsampling"
mkdir "output/nn_upsampling"
for filename in data/*/view1.png; do
    mkdir "output/bilinear_upsampling/$(echo $filename | cut -f 2 -d '/')"
    mkdir "output/cubic_upsampling/$(echo $filename | cut -f 2 -d '/')"
    mkdir "output/nn_upsampling/$(echo $filename | cut -f 2 -d '/')"

    n1="output/bilinear_upsampling/$(echo $filename | cut -f 2 -d '/')/downsampled.png"
    n2="output/cubic_upsampling/$(echo $filename | cut -f 2 -d '/')/downsampled.png"
    n3="output/nn_upsampling/$(echo $filename | cut -f 2 -d '/')/downsampled.png"

    ./build/filters 5 $filename $n1 "output/bilinear_upsampling/$(echo $filename | cut -f 2 -d '/')/upsampled.png" 7 0 $i "$(dirname $(echo $filename))/disp1.png"
    ./build/filters 6 $filename $n2 "output/cubic_upsampling/$(echo $filename | cut -f 2 -d '/')/upsampled.png" 7 0 $i "$(dirname $(echo $filename))/disp1.png"
    ./build/filters 7 $filename $n3 "output/nn_upsampling/$(echo $filename | cut -f 2 -d '/')/upsampled.png" 7 0 $i "$(dirname $(echo $filename))/disp1.png"
done


# Bilateral joint upsampling
mkdir "output/bilateral_joint_upsampling"
for filename in data/*/view1.png; do
    mkdir "output/bilateral_joint_upsampling/$(echo $filename | cut -f 2 -d '/')"

    declare -a arr=("0.4" "0.8" "1.2" "2")
    declare -a arr2=("10" "15" "20" "25")

    n="output/bilateral_joint_upsampling/$(echo $filename | cut -f 2 -d '/')/downsampled.png"

    for i in "${arr[@]}"; do
        for j in "${arr2[@]}"; do
            ./build/filters 8 $filename $n "output/bilateral_joint_upsampling/$(echo $filename | cut -f 2 -d '/')/upsampled_k_7_s1_$i s2_$j.png" 7 $i $j "$(dirname $(echo $filename))/disp1.png"
        done
    done
done


mkdir "output/bilateral_joint_median_upsampling"
for filename in data/*/view1.png; do
    mkdir "output/bilateral_joint_median_upsampling/$(echo $filename | cut -f 2 -d '/')"

    n="output/bilateral_joint_median_upsampling/$(echo $filename | cut -f 2 -d '/')/downsampled.png"

    declare -a arr2=("10" "15" "20" "25")

    for j in "${arr2[@]}"; do
        ./build/filters 9 $filename $n "output/bilateral_joint_median_upsampling/$(echo $filename | cut -f 2 -d '/')/upsampled_k_7_s_$j.png" 7 0 $j "$(dirname $(echo $filename))/disp1.png"
    done
done
'''
