cmake_minimum_required(VERSION 3.9)

project( filters )

###############
# packages #
###############

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

find_package(OpenMP)

###############
# executables #
###############

add_executable( filters src/main.cpp )
target_link_libraries( filters ${OpenCV_LIBS} )

if(OpenMP_CXX_FOUND)
    target_link_libraries(filters OpenMP::OpenMP_CXX)
endif()