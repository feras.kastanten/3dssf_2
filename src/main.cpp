#include <iostream>
#include <opencv2/opencv.hpp>
#include <array>

cv::Mat CreateGaussianKernel(const int window_size)
{
    cv::Mat kernel(cv::Size(window_size, window_size), CV_64F);

    const int half_window_size = window_size / 2;

    // see: lecture_03_slides.pdf, Slide 13
    const double k = 2.5;
    const double r_max = std::sqrt(2.0 * half_window_size * half_window_size);
    const double sigma = r_max / k;

    // sum is for normalization
    double sum = 0.0;

    for (int x = -window_size / 2; x <= window_size / 2; x++)
    {
        for (int y = -window_size / 2; y <= window_size / 2; y++)
        {
            double val = exp(-(x * x + y * y) / (2 * sigma * sigma));
            kernel.at<double>(x + window_size / 2, y + window_size / 2) = val;
            sum += val;
        }
    }

    kernel = kernel / sum;

    return kernel;
}

cv::Mat CreateGaussianKernel(const int window_size, const double sigma)
{
    cv::Mat kernel(cv::Size(window_size, window_size), CV_64F);

    const int half_window_size = window_size / 2;

    double sum = 0.0;

    for (int x = -half_window_size; x <= half_window_size; x++)
    {
        for (int y = -half_window_size; y <= half_window_size; y++)
        {
            double val = exp(-(x * x + y * y) / (2 * sigma * sigma));
            kernel.at<double>(x + half_window_size, y + half_window_size) = val;
            sum += val;
        }
    }

    kernel = kernel / sum;

    return kernel;
}

void OurFiler_Gaussian(const cv::Mat &input, cv::Mat &output, const int window_size = 5)
{

    const auto width = input.cols;
    const auto height = input.rows;

    cv::Mat gaussianKernel = CreateGaussianKernel(window_size);

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {

            int sum = 0;
            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {
                    sum += input.at<uchar>(r + i, c + j) * gaussianKernel.at<double>(i + window_size / 2, j + window_size / 2);
                }
            }
            output.at<uchar>(r, c) = sum;
        }
    }
}

void filtering_with_kernal(const cv::Mat &input, const cv::Mat &kernal, cv::Mat &output)
{
    const auto width = input.cols;
    const auto height = input.rows;

    const int window_size = kernal.rows;

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {

            int sum = 0;
            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {
                    sum += input.at<uchar>(r + i, c + j) * kernal.at<double>(i + window_size / 2, j + window_size / 2);
                }
            }
            output.at<uchar>(r, c) = sum;
        }
    }
}

void filtering_with_bilateral_filter(
    const cv::Mat &input,
    const cv::Mat &kernal,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const auto width = input.cols;
    const auto height = input.rows;
    const int window_size = kernal.rows;

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {

            int sum = 0;
            double sum_weights = 0.0;

            const auto input_central = input.at<uchar>(r, c);

            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {

                    auto input_other = input.at<uchar>(r + i, c + j);
                    //double spectral_difference = std::pow(input_other - input_central, 2);
                    double weight =
                        p(input_other, input_central) *
                        kernal.at<double>(i + window_size / 2, j + window_size / 2);

                    sum += input_other * weight;

                    sum_weights += weight;
                }
            }

            output.at<uchar>(r, c) = sum / sum_weights;
        }
    }
}

void filtering_with_joint_bilateral_filter(
    const cv::Mat &input,
    const cv::Mat &guide,
    const cv::Mat &kernal,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const auto width = input.cols;
    const auto height = input.rows;
    const int window_size = kernal.rows;

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {

            int sum = 0;
            double sum_weights = 0.0;

            const auto guide_central = guide.at<uchar>(r, c);

            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {

                    auto guide_other = guide.at<uchar>(r + i, c + j);
                    auto input_other = input.at<uchar>(r + i, c + j);
                    //double spectral_difference = std::pow(input_other - input_central, 2);
                    //double spectral_difference = std::abs(guide_central - guide_other);
                    double weight =
                        p(guide_central, guide_other) *
                        kernal.at<double>(i + window_size / 2, j + window_size / 2);

                    sum += input_other * weight;

                    sum_weights += weight;
                }
            }

            output.at<uchar>(r, c) = sum / sum_weights;
        }
    }
}

void filtering_with_bilateral_median_filter(
    const cv::Mat &input,
    const int window_size,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const auto width = input.cols;
    const auto height = input.rows;

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {
            double sum_weights = 0.0;

            const auto input_central = input.at<uchar>(r, c);

            std::vector<std::pair<uchar, double>> arr;

            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {

                    auto input_other = input.at<uchar>(r + i, c + j);
                    //double spectral_difference = std::pow(input_other - input_central, 2);
                    //double spectral_difference = std::abs(input_other - input_central);
                    double weight = p(input_other, input_central);

                    arr.push_back(std::make_pair(input_other, weight));

                    sum_weights += weight;
                }
            }

            // weighted median
            std::sort(arr.begin(), arr.end());
            uchar val;
            double acc = 0.;
            for (int i = 0; i < arr.size(); i++)
            {
                acc += arr[i].second / sum_weights;
                if (acc >= 0.5)
                {
                    val = arr[i].first;
                    break;
                }
            }

            output.at<uchar>(r, c) = val;
        }
    }
}

void filtering_with_joint_bilateral_median_filter(
    const cv::Mat &input,
    const cv::Mat &guide,
    const int window_size,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const auto width = input.cols;
    const auto height = input.rows;

    output.create(height, width, CV_8U);

    for (int r = window_size / 2; r < height - window_size / 2; ++r)
    {
        for (int c = window_size / 2; c < width - window_size / 2; ++c)
        {
            double sum_weights = 0.0;

            const auto guide_central = guide.at<uchar>(r, c);

            std::vector<std::pair<uchar, double>> arr;

            for (int i = -window_size / 2; i <= window_size / 2; ++i)
            {
                for (int j = -window_size / 2; j <= window_size / 2; ++j)
                {

                    auto input_other = input.at<uchar>(r + i, c + j);
                    auto guide_other = guide.at<uchar>(r + i, c + j);
                    //double spectral_difference = std::pow(input_other - input_central, 2);
                    //double spectral_difference = std::abs(input_other - input_central);
                    double weight = p(guide_other, guide_central);

                    arr.push_back(std::make_pair(input_other, weight));

                    sum_weights += weight;
                }
            }

            // weighted median
            std::sort(arr.begin(), arr.end());
            uchar val;
            double acc = 0.;
            for (int i = 0; i < arr.size(); i++)
            {
                acc += arr[i].second / sum_weights;
                if (acc >= 0.5)
                {
                    val = arr[i].first;
                    break;
                }
            }

            output.at<uchar>(r, c) = val;
        }
    }
}

void guided_gaussian_iterative_upsampling(
    const cv::Mat &image,
    const cv::Mat &guide,
    const int factor,
    const cv::Mat &kernal,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const int lg = std::round(std::log2(factor));
    const int guide_width = guide.cols;
    const int guide_height = guide.rows;
    const int window_size = kernal.rows;

    cv::Mat sofar_output = image.clone();
    for (int it = 0; it < lg; it++)
    {
        const int last_width = sofar_output.cols;
        const int last_height = sofar_output.rows;

        const int new_width = it == lg - 1 ? guide.cols : last_width * 2;
        const int new_height = it == lg - 1 ? guide.rows : last_height * 2;

        cv::Mat cur_output(new_height, new_width, CV_8U);

        for (int r = window_size / 2; r < new_height - window_size / 2; ++r)
        {
            std::cout << "row #" << r << "/" << new_height << std::endl;
            for (int c = window_size / 2; c < new_width - window_size / 2; ++c)
            {
                double sum_weights = 0.0;
                double sum = 0;
                const auto guide_central =
                    guide.at<uchar>(std::round(1.0 * r * guide_height / new_height), std::round(1.0 * c * guide_width / new_width));

                for (int i = -window_size / 2; i <= window_size / 2; ++i)
                {
                    for (int j = -window_size / 2; j <= window_size / 2; ++j)
                    {

                        auto input_other = sofar_output.at<uchar>(std::round(1.0 * (r + i) * last_height / new_height), std::round(1.0 * (c + j) * last_width / new_width));
                        auto guide_other = guide.at<uchar>(std::round(1.0 * (r + i) * guide_height / new_height), std::round(1.0 * (c + j) * guide_width / new_width));

                        double weight = p(guide_other, guide_central) *
                                        kernal.at<double>(i + window_size / 2, j + window_size / 2);

                        sum += input_other * weight;

                        sum_weights += weight;
                    }
                }

                cur_output.at<uchar>(r, c) = (uchar)(sum / sum_weights);
            }
        }

        cur_output.copyTo(sofar_output);
    }

    sofar_output.copyTo(output);
}

void guided_median_iterative_upsampling(
    const cv::Mat &image,
    const cv::Mat &guide,
    const int factor,
    const int window_size,
    const std::function<double(const double, const double)> &p,
    cv::Mat &output)
{
    const int lg = std::round(std::log2(factor));
    const int guide_width = guide.cols;
    const int guide_height = guide.rows;

    cv::Mat sofar_output = image.clone();
    for (int it = 0; it < lg; it++)
    {
        const int last_width = sofar_output.cols;
        const int last_height = sofar_output.rows;

        const int new_width = it == lg - 1 ? guide.cols : last_width * 2;
        const int new_height = it == lg - 1 ? guide.rows : last_height * 2;

        cv::Mat cur_output(new_height, new_width, CV_8U);

        for (int r = window_size / 2; r < new_height - window_size / 2; ++r)
        {
            std::cout << "row #" << r << "/" << new_height << std::endl;
            for (int c = window_size / 2; c < new_width - window_size / 2; ++c)
            {
                double sum_weights = 0.0;

                const auto guide_central =
                    guide.at<uchar>(std::round(1.0 * r * guide_height / new_height), std::round(1.0 * c * guide_width / new_width));

                std::vector<std::pair<uchar, double>> arr;

                for (int i = -window_size / 2; i <= window_size / 2; ++i)
                {
                    for (int j = -window_size / 2; j <= window_size / 2; ++j)
                    {

                        auto input_other = sofar_output.at<uchar>(std::round(1.0 * (r + i) * last_height / new_height), std::round(1.0 * (c + j) * last_width / new_width));
                        auto guide_other = guide.at<uchar>(std::round(1.0 * (r + i) * guide_height / new_height), std::round(1.0 * (c + j) * guide_width / new_width));

                        double weight = p(guide_other, guide_central);

                        arr.push_back(std::make_pair(input_other, weight));

                        sum_weights += weight;
                    }
                }

                // weighted median
                std::sort(arr.begin(), arr.end());
                uchar val;
                double acc = 0.;
                for (int i = 0; i < arr.size(); i++)
                {
                    acc += arr[i].second / sum_weights;
                    if (acc >= 0.5)
                    {
                        val = arr[i].first;
                        break;
                    }
                }

                cur_output.at<uchar>(r, c) = val;
            }
        }

        cur_output.copyTo(sofar_output);
    }

    sofar_output.copyTo(output);
}

int evaluation(
    const cv::Mat &disparites,
    const cv::Mat &ground_truth)
{
    const int image_width = disparites.cols;
    const int image_height = disparites.rows;

    int err = 0;
    int cnt = 0;
    for (int i = 0; i < image_height; i++)
    {
        for (int j = 0; j < image_width; j++)
        {
            if (disparites.at<uchar>(i, j) != 0 && ground_truth.at<uchar>(i, j) != 0)
            {
                const int diff = disparites.at<uchar>(i, j) - ground_truth.at<uchar>(i, j);
                err += std::abs(diff);
                cnt++;
            }
        }
    }
    std::cout << "err: " << err << std::endl;
    err /= cnt;

    return err;
}

int main(int argc, char **argv)
{
    const int task = std::stoi(argv[1]);
    const std::string image_path = argv[2];
    const std::string output_path1 = argv[3];
    const std::string output_path2 = argv[4];
    const int kernal_size = std::stoi(argv[5]);
    const double sigma1 = std::stod(argv[6]);
    const double sigma2 = argc > 7 ? std::stod(argv[7]) : 0;
    const std::string disparity_path = argc > 8 ? argv[8] : "";

    cv::Mat image = cv::imread(image_path, cv::IMREAD_GRAYSCALE);

    cv::Mat depth_image;
    if (disparity_path != "")
        depth_image = cv::imread(disparity_path, cv::IMREAD_GRAYSCALE);

    // Generate noise image
    cv::Mat noise(image.size(), image.type());
    uchar mean = 0;
    uchar stddev = 10;
    cv::randn(noise, mean, stddev);
    cv::Mat noisy_image = image + noise;

    // Resize depth image
    const int fact = 4;
    cv::Mat depth_resized;
    if (!depth_image.empty())
    {
        cv::resize(depth_image,
                   depth_resized,
                   cv::Size(0, 0),
                   1. / fact,
                   1. / fact);
    }

    auto p = [sigma2](const double x1, const double x2)
    {
        const double spectral_difference = std::pow(x1 - x2, 2);
        //const double spectral_difference = std::abs(x1 - x2);
        return std::exp(-spectral_difference / sigma2);
    };

    if (task == 0)
    {
        cv::Mat kernal = CreateGaussianKernel(kernal_size, sigma1);

        cv::Mat output;
        filtering_with_kernal(noisy_image, kernal, output);

        cv::imwrite(output_path1, noisy_image);
        cv::imwrite(output_path2, output);
    }
    else if (task == 1)
    {
        cv::Mat kernal = CreateGaussianKernel(kernal_size, sigma1);

        cv::Mat output_bilitral;
        filtering_with_bilateral_filter(noisy_image, kernal, p, output_bilitral);

        cv::imwrite(output_path1, noisy_image);
        cv::imwrite(output_path2, output_bilitral);
    }
    else if (task == 2)
    {
        cv::Mat kernal = CreateGaussianKernel(kernal_size, sigma1);

        cv::Mat output_bilitral;
        filtering_with_joint_bilateral_filter(noisy_image, depth_image, kernal, p, output_bilitral);

        cv::imwrite(output_path1, noisy_image);
        cv::imwrite(output_path2, output_bilitral);
    }
    else if (task == 3)
    {
        cv::Mat output_bilitral;
        filtering_with_bilateral_median_filter(noisy_image, kernal_size, p, output_bilitral);

        cv::imwrite(output_path1, noisy_image);
        cv::imwrite(output_path2, output_bilitral);
    }
    else if (task == 4)
    {
        cv::Mat output_bilitral;
        filtering_with_joint_bilateral_median_filter(noisy_image, depth_image, kernal_size, p, output_bilitral);

        cv::imwrite(output_path1, noisy_image);
        cv::imwrite(output_path2, output_bilitral);
    }
    else if (task == 5)
    {
        cv::Mat bilinear_inter;
        cv::resize(depth_resized, bilinear_inter, image.size(), 0., 0., cv::INTER_LINEAR);

        cv::imwrite(output_path1, depth_resized);
        cv::imwrite(output_path2, bilinear_inter);
    }
    else if (task == 6)
    {
        cv::Mat cubic_inter;
        cv::resize(depth_resized, cubic_inter, image.size(), 0., 0., cv::INTER_CUBIC);

        cv::imwrite(output_path1, depth_resized);
        cv::imwrite(output_path2, cubic_inter);
    }
    else if (task == 7)
    {
        cv::Mat nn_inter;
        cv::resize(depth_resized, nn_inter, image.size(), 0., 0., cv::INTER_NEAREST);

        cv::imwrite(output_path1, depth_resized);
        cv::imwrite(output_path2, nn_inter);
    }
    else if (task == 8)
    {
        cv::Mat kernal = CreateGaussianKernel(kernal_size, sigma1);

        cv::Mat upsampled;
        guided_gaussian_iterative_upsampling(
            depth_resized,
            image,
            fact,
            kernal,
            p,
            upsampled);

        cv::imwrite(output_path1, depth_resized);
        cv::imwrite(output_path2, upsampled);
    }
    else if (task == 9)
    {
        cv::Mat kernal = CreateGaussianKernel(kernal_size, sigma1);

        cv::Mat upsampled;
        guided_median_iterative_upsampling(
            depth_resized,
            image,
            fact,
            kernal_size,
            p,
            upsampled);

        cv::imwrite(output_path1, depth_resized);
        cv::imwrite(output_path2, upsampled);
    }

    /*
    std::cout << "eval upsampled: " << evaluation(depth_image, upsampled) << std::endl;
    std::cout << "eval bilinear_inter: " << evaluation(depth_image, bilinear_inter) << std::endl;
    std::cout << "eval nn_inter: " << evaluation(depth_image, nn_inter) << std::endl;
    std::cout << "eval cubic_inter: " << evaluation(depth_image, cubic_inter) << std::endl;*/

    //cv::waitKey(100000);

    return 0;
}